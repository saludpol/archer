# -*- coding: utf-8 -*-

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
# request.requires_https()

if not request.env.web2py_runtime_gae:
    ## if NOT running on Google App Engine use SQLite or other DB
    #db = DAL('sqlite://storage.sqlite',pool_size=1,check_reserved=['all'])
    db = DAL("postgres://saludpol:y2KSalud14@localhost:5432/archer")
else:
    ## connect to Google BigTable (optional 'google:datastore://namespace')
    db = DAL('google:datastore')
    ## store sessions and tickets there
    session.connect(request, response, db=db)
    ## or store session in Memcache, Redis, etc.
    ## from gluon.contrib.memdb import MEMDB
    ## from google.appengine.api.memcache import Client
    ## session.connect(request, response, db = MEMDB(Client()))

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'
## (optional) static assets folder versioning
# response.static_version = '0.0.0'
#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################
import datetime
from gluon.tools import Auth, Crud, Service, PluginManager, prettydate

settings.DEVELOPMENT = False
if request.env.http_host in ['127.0.0.1:8000', 'localhost:8000']:
    settings.DEVELOPMENT = True

cas_provider = 'http://192.168.0.246/provider/default/user/cas'
if settings.DEVELOPMENT:
    cas_provider = 'http://localhost:8000/provider/default/user/cas'

# auth = Auth(db, cas_provider=cas_provider)
auth = Auth(db)
crud, service, plugins = Crud(db), Service(), PluginManager()
T.force('es')

## create all tables needed by auth if not custom tables
#auth.define_tables(username=False, signature=False)

## configure email
mail = auth.settings.mailer
mail.settings.server = 'logging' or 'smtp.gmail.com:587'
mail.settings.sender = 'you@gmail.com'
mail.settings.login = 'username:password'

## configure auth policy
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key
from gluon.contrib.login_methods.rpx_account import use_janrain
use_janrain(auth, filename='private/janrain.key')

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

## after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)

mail.settings.server = settings.email_server
mail.settings.sender = settings.email_sender
mail.settings.login = settings.email_login

now = datetime.datetime.now()

status_options = {
    0: T('Active'),
    1: T('Inactive')
}

hospitalization_options = {
    0: T('Input'),
    1: T('Output')
}

gender_options = {
    0: T('Male'),
    1: T('Female')
}

db.define_table('medical_centers',
                Field('code', 'string', label=T('Code')),
                Field('name', 'string', label=T('Name')),
                Field('address', 'string', label=T('Address')),
                Field('url_location', 'string', label=T('Url Location')),
                format='%(name)s')

auth.settings.extra_fields['auth_user'] = [
    Field('phone', label=T('Phone')),
    Field('medical_center', db.medical_centers, label=T('Medical Center'),
          requires=IS_EMPTY_OR(IS_IN_DB(db, db.medical_centers, '%(name)s'))),
]

auth.define_tables(username=False, signature=False)

# db.define_table('account_status',
#                 Field('name', 'string', label=T('Name')),
#                 format='%(name)s')

db.define_table('regimens',
                Field('name', 'string', label=T('Name')),
                format='%(name)s')

db.define_table('roles',
                Field('name', 'string', label=T('Name')),
                format='%(name)s')

db.define_table('conditions',
                Field('name', 'string', label=T('Name')),
                format='%(name)s')

db.define_table('relations',
                Field('name', 'string', label=T('Name')),
                format='%(name)s')

# db.define_table('studies',
#                 Field('name', 'string', label=T('Name')),
#                 format='%(name)s')

db.define_table('units_measures',
                Field('name', 'string', label=T('Name')),
                format='%(name)s')

db.define_table('specialities',
                Field('code', 'string', label=T('Code')),
                Field('name', 'string', label=T('Name')),
                format='%(name)s')

db.define_table('services',
                Field('name', 'string', label=T('Name')),
                Field('speciality', 'reference specialities',
                      label=T('Speciality')),
                format='%(name)s')

db.define_table('services_prices',
                Field('register_time', 'datetime', label=T('Register Time'),
                      default=now, writable=False),
                Field('service', 'reference services',
                      label=T('Service')),
                Field('price', 'float', default=0, label=T('Price')),
                format='%(id)s')

db.define_table('procedures',
                Field('name', 'string', label=T('Name')),
                Field('speciality', 'reference specialities',
                      label=T('Speciality')),
                format='%(name)s')

db.define_table('specialists',
                Field('medical_id', 'string', label=T('Medical ID')),
                Field('document_id', 'string', label=T('DNI')),
                Field('full_name', 'string', label=T('Name')),
                Field('speciality', 'reference specialities',
                      label=T('Speciality')),
                format='%(full_name)s')

db.define_table('cie',
                Field('code', 'string', label=T('CIE Code')),
                Field('name', 'string', label=T('Name')),
                format='%(name)s')

db.define_table('insurance_plans',
                Field('name', 'string', label=T('Name')),
                Field('monthly_fee', 'float', default=0, label=T('Fee')),
                format='%(name)s')

db.define_table('insurance_coverage',
                Field('insurance_plan', 'reference insurance_plans',
                      label=T('Plan')),
                Field('service', 'reference services', label=T('Services')),
#                Field('name', 'string', label=T('Name')),
                Field('coverage', 'float', default=100, label=T('Coverage'),
                      comment=T('%')),
                Field('coverage_fee', 'float', default=0, label=T('Fee')),
                format='%(id)s')

db.define_table('drugs',
                Field('code', 'string', label=T('Drug Code')),
                Field('name', 'string', label=T('Name')),
                Field('concentration', 'string', label=T('Concentration')),
                Field('way', 'string', label=T('Way')),
                Field('presentation', 'string', label=T('Presentation')),
                format='%(name)s - %(concentration)s - %(way)s')

db.define_table('drugs_relations',
                Field('drug', 'reference drugs',
                      label=T('Drug')),
                Field('speciality', 'reference specialities',
                      label=T('Speciality')),
                format='%(id)s')

db.define_table('drugs_prices',
                Field('register_time', 'datetime', label=T('Register Time'),
                      default=now, writable=False),
                Field('drug', 'reference drugs',
                      label=T('Drug')),
                Field('price', 'float', default=0, label=T('Price')),
                format='%(id)s')

db.define_table('affiliates',
                Field('document_id', 'string', label=T('DNI')),
                Field('alternate_document_id', 'string',
                      label=T('Alternate ID')),
                Field('additional_document_id', 'string',
                      label=T('Additional ID')),
                Field('full_name', 'string', label=T('Full Name')),
                Field('father_last_name', 'string',
                       label=T('Father Last Name')),
                Field('mother_last_name', 'string',
                       label=T('Mother Last Name')),
                Field('birth_date', 'date', label=T('Birth Date')),
                Field('gender', 'integer', label=T('Gender'), default=0,
                      requires=IS_IN_SET(gender_options)),
                Field('affiliate_plan', 'reference insurance_plans',
                      label=T('Plan')),
                Field('registration_date', 'date', label=T('Inscription Date')),
                Field('holder', 'boolean', label=T('Holder')),
#                Field('reference_customer', label=T('Cuenta Referencia')),
                Field('status', 'integer', label=T('Status'), default=0,
                      requires=IS_IN_SET(status_options)),
#                format='%(document_id)s - %(full_name)s %(father_last_name)s %(mother_last_name)s')
                format='%(document_id)s - %(full_name)s')

db.define_table('affiliates_relations',
                Field('affiliate', 'reference affiliates', label=T('Affiliate'),
                      writable=False, readable=False),
                Field('relation', 'reference relations', label=T('Relation')),
                Field('related_affiliate', 'reference affiliates',
                      label=T('Related Affiliate')),
                format='%(affiliate)s')

db.define_table('affiliates_documents',
                Field('affiliate', 'reference affiliates', label=T('Affiliate'),
                      writable=False, readable=False),
                Field('name', 'string', label=T('Name')),
                Field('document_date', 'date', label=T('Document Date')),
                Field('document_file', 'upload', label=T('Document')),
                format='%(affiliate)s')

db.define_table('affiliates_properties',
                Field('affiliate', 'reference affiliates', label=T('Affiliate'),
                      writable=False, readable=False),
                Field('role', 'reference roles', label=T('Role')),
                Field('affiliate_condition', 'reference conditions',
                      label=T('Condition')),
                Field('regimen', 'reference regimens', label=T('Regimen')),
                format='%(affiliate)s')

db.define_table('affiliates_locations',
                Field('affiliate', 'reference affiliates', label=T('Affiliate'),
                      writable=False, readable=False),
                Field('address', 'string', label=T('Address')),
                Field('address_url', 'string', label=T('Geo Referenced Address')),
                Field('document_file', 'upload', label=T('Document')),
                format='%(affiliate)s')

db.define_table('affiliates_phones',
                Field('affiliate', 'reference affiliates', label=T('Affiliate'),
                      writable=False, readable=False),
                Field('name', 'string', label=T('Name')),
                Field('phone', 'string', label=T('Phone')),
                format='%(affiliate)s')

db.define_table('affiliates_mails',
                Field('affiliate', 'reference affiliates', label=T('Affiliate'),
                      writable=False, readable=False),
                Field('name', 'string', label=T('Name')),
                Field('mail', 'string', label=T('Mail')),
                format='%(affiliate)s')

db.define_table('operations',
                Field('register_time', 'datetime', label=T('Register Time'),
                      default=now, writable=False),
                Field('affiliate', label=T('Affiliate')),
                Field('operation_date', 'date', label=T('Operation Date'),
                      default=now),
                Field('medical_center', 'reference medical_centers',
                      label=T('Medical Center')),
                Field('internal_document', 'string',
                      label=T('Reference Document')),
#                Field('diagnosis', label=T('Diagnosis')),
                Field('speciality', 'reference specialities',
                      label=T('Speciality')),
                Field('specialist', 'reference specialists',
                      label=T('Specialist')),
                Field('status', 'integer', label=T('Status'), default=0,
                      requires=IS_IN_SET(status_options)),
                format='%(id)s')

db.define_table('imagenology',
                Field('code', 'string', label=T('Code')),
                Field('name', 'string', label=T('Name')),
                format='%(name)s')

db.define_table('tests',
                Field('code', 'string', label=T('Code')),
                Field('name', 'string', label=T('Name')),
                format='%(name)s')

db.define_table('operations_diagnosis',
                Field('register_time', 'datetime', label=T('Register Time'),
                      default=now, writable=False),
                Field('related_operation', 'reference operations',
                      label=T('Operation'), writable=False, readable=False),
                Field('diagnosis', label=T('Diagnosis')),
                Field('description', 'text',label=T('Description')),
                format='%(related_operation)s')

db.define_table('operations_prescriptions',
                Field('register_time', 'datetime', label=T('Register Time'),
                      default=now, writable=False),
                Field('related_operation', 'reference operations',
                      label=T('Operation'),
                      writable=False, readable=False),
                Field('drug', 'reference drugs', label=T('Drug')),
#                Field('unit', 'reference units_measures',label=T('Unit')),
                Field('quantity', 'float',label=T('Quantity')),
#                Field('quantity', 'float',label=T('Quantity')),
                Field('indication', 'text',label=T('Indications')),
                format='%(related_operation)s')

# db.define_table('operations_studies',
#                 Field('register_time', 'datetime', label=T('Register Time'),
#                       default=now, writable=False),
#                 Field('related_operation', 'reference operations',
#                       label=T('Operation'), writable=False, readable=False),
#                 Field('study', 'reference studies',label=T('Study')),
#                 Field('indication', 'text',label=T('Indications')),
#                 format='%(related_operation)s')

db.define_table('operations_procedures',
                Field('register_time', 'datetime', label=T('Register Time'),
                      default=now, writable=False),
                 Field('related_operation', 'reference operations',
                       label=T('Operation'), writable=False, readable=False),
                Field('related_procedure', 'reference procedures',
                      label=T('Procedure')),
                Field('indication', 'text',label=T('Indications')),
                format='%(related_operation)s')

db.define_table('operations_hospitalization',
                Field('register_time', 'datetime', label=T('Register Time'),
                      default=now, writable=False),
                Field('related_operation', 'reference operations',
                      label=T('Operation'), writable=False, readable=False),
                Field('register_mode', 'integer', label=T('Status'), default=0,
                      requires=IS_IN_SET(hospitalization_options)),
                Field('related_date', 'date', label=T('Related Date'), requires=IS_NOT_EMPTY(error_message='Ingrese una fecha')),
                Field('indication', 'text',label=T('Indications')),
                format='%(related_operation)s')

db.define_table('operations_laboratories',
                Field('register_time', 'datetime', label=T('Register Time'),
                    default=now, writable=False),
                Field('related_operation', 'reference operations',
                      label=T('Operation'), writable=False, readable=False),
                Field('related_test', 'reference tests',
                      label=T('Medical test')),
                Field('indication', 'text',label=T('Indications')),
                format='%(related_operation)s')

db.define_table('operations_imagenology',
                Field('register_time', 'datetime', label=T('Register Time'),
                    default=now, writable=False),
                Field('related_operation', 'reference operations',
                      label=T('Operation'), writable=False, readable=False),
                Field('related_imagenology', 'reference imagenology',
                      label=T('Imagenology')),
                Field('indication', 'text',label=T('Indications')),
                format='%(related_operation)s')


#auth.enable_record_versioning(db)
def name_data(table, id_val, field='name'):
    """
    Content Data Representation
    """
    try:
        value_data = table(id_val)[field]
    except:
        value_data = ''
    data = '%s' % value_data
    data = data.decode('utf8')
    return data


# db.operations.affiliate.widget = SQLFORM.widgets.autocomplete(request,
#     db.affiliates.document_id, limitby=(0, 10),
#     id_field=db.affiliates.id, min_length=2)
# db.operations_diagnosis.diagnosis.widget = SQLFORM.widgets.autocomplete(request,
#     db.cie.name, limitby=(0, 10),
#     id_field=db.cie.id, min_length=2)
db.operations.affiliate.represent = lambda  value, row: None if value is None else name_data(
    db.affiliates, value, 'full_name')
# db.operations.medical_center.widget = SQLFORM.widgets.autocomplete(request,
#     db.medical_centers.name, limitby=(0, 10),
#     id_field=db.medical_centers.id, min_length=2)
# db.operations.service.widget = SQLFORM.widgets.autocomplete(request,
#     db.services.name, limitby=(0, 10),
#     id_field=db.services.id, min_length=2)
# db.operations.specialist.widget = SQLFORM.widgets.autocomplete(request,
#     db.specialists.full_name, limitby=(0, 10),
#     id_field=db.specialists.id, min_length=2)
# db.operations_prescriptions.drug.widget = SQLFORM.widgets.autocomplete(request,
#     db.drugs.name, limitby=(0, 10),
#     id_field=db.drugs.id, min_length=2)
# db.affiliates_relations.related_affiliate.widget = SQLFORM.widgets.autocomplete(request,
#     db.affiliates.document_id, limitby=(0, 10),
#     id_field=db.affiliates.id, min_length=2)
db.operations_diagnosis.diagnosis.represent = lambda  value, row: None if value is None else name_data(db.cie, value)
db.affiliates.gender.represent = lambda  value, row: None if value is None else gender_options[value]
db.affiliates.status.represent = lambda  value, row: None if value is None else status_options[value]
db.operations.status.represent = lambda  value, row: None if value is None else status_options[value]
#db.operations.affiliate.widget = autocomplete_widget(tablename='affiliates',fieldname='document_id')
db.affiliates_relations.related_affiliate.widget = SQLFORM.widgets.autocomplete(request, db.affiliates.full_name, id_field=db.affiliates.id)
db.operations.affiliate.widget = SQLFORM.widgets.autocomplete(request, db.affiliates.full_name, id_field=db.affiliates.id)
db.operations.medical_center.widget = SQLFORM.widgets.autocomplete(request, db.medical_centers.name, id_field=db.medical_centers.id)
# db.operations.service.widget = lambda field,value: autocomplete_widget(field,value,base='services',content='name')
db.operations.specialist.widget = SQLFORM.widgets.autocomplete(request, db.specialists.full_name, id_field=db.specialists.id)
db.operations_prescriptions.drug.widget = lambda field, value: autocomplete_widget(field, value, base='drugs', content='code,name')
db.operations_diagnosis.diagnosis.widget = SQLFORM.widgets.autocomplete(request, db.operations_diagnosis.diagnosis, id_field=db.operations_diagnosis.id)
db.operations_procedures.related_procedure.widget = lambda field, value: autocomplete_widget(field, value, base='procedures', content='name')
db.operations_imagenology.related_imagenology.widget = lambda field, value: autocomplete_widget(field, value, base='imagenology', content='code,name')
db.operations_laboratories.related_test.widget = lambda field, value: autocomplete_widget(field, value, base='tests', content='code,name')
