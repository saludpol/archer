from gluon.storage import Storage
settings = Storage()

settings.migrate = True
settings.title = 'SIGE - SALUDPOL'
settings.subtitle = ''
settings.author = 'Alfonso de la Guarda Reyes'
settings.author_email = 'alfonsodg@gmail.com'
settings.keywords = ''
settings.description = ''
settings.layout_theme = 'Default'
settings.database_uri = 'sqlite://storage.sqlite'
settings.security_key = 'bbf44ebf-c658-48ab-b82d-e6e1b5748cc0'
settings.email_server = 'localhost'
settings.email_sender = 'you@example.com'
settings.email_login = ''
settings.login_method = 'local'
settings.login_config = ''
settings.plugins = []

try:
    UPLOAD_FOLDER = request.wsgi.environ['REQUEST_URI'].split('/')[1]
except:
    UPLOAD_FOLDER = ''
UPLOAD_PATH = 'applications/%s/uploads' % UPLOAD_FOLDER
EXCEL_FILE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
OLD_EXCEL_FILE = 'application/vnd.ms-excel'
CSV_FILE = 'text/csv'
