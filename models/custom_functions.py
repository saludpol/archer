__author__ = 'alfonsodg'
import uuid


def autocomplete_widget(field, value, base=False,
        content=False, format=False):
    id = "autocomplete-" + str(uuid.uuid4())
    callback_url = URL(r=request,c='default',f='get_items')
    wrapper = DIV(_id=id)
    #input = SQLFORM.widgets.string.widget(field,value)
    key_id = "_autocomplete_%s_%s_aux" % (base, field.name)
    if value is not None and value != '':
        fields = [db[base][fd] for fd in content.split(',')]
        values = db[base](db[base]['id']==value)#.select(*fields)
        tmp = []
        for fd in fields:
            tmp.append(values[fd])
        value_aux = "-".join(tmp)
    else:
        value_aux = ""
    input_aux = INPUT(_name="_autocomplete_%s_%s_aux" % (base, field.name),
                      _id="%s_%s" % (field._tablename, field.name),
                      _class=field.type, _type='text',
                      _value=value_aux)
    input = INPUT(_id=key_id, _name="%s" % field.name,
                  _type='hidden', _value=value)
    script = SCRIPT("""jQuery('#%s input').autocomplete({
        source: function( request, response ) {
            $.ajax({
              url: "%s",
              dataType: "json",
              data: {
                q: request.term,
                base: '%s',
                content: '%s',
              },
              success: function( data ) {
                response( $.map( data, function(item) {
                    return {
                        value: item.value,
                        uuid: item.uuid
                    }
                 }));
              }
            });
         },
        minLength: 2,
        select: function( event, ui ) {
            $("#%s").val(ui.item.uuid);
        }
      });""" % (id, callback_url, base, content, key_id))
    wrapper.append(input_aux)
    wrapper.append(script)
    wrapper.append(input)
    return wrapper