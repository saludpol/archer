response.title = settings.title
response.subtitle = settings.subtitle
response.meta.author = '%(author)s <%(author_email)s>' % settings
response.meta.keywords = settings.keywords
response.meta.description = settings.description
home = [
(T('Index'),URL('default','index')==URL(),URL('default','index'),[]),
]
configuration = [
(T('Configuration'), False, None,
 [
     (T('Medical Centers'), False, URL('config', 'medical_center'),[]),
     (T('Roles'), False, URL('config', 'roles'),[]),
     (T('Conditions'), False, URL('config', 'conditions'),[]),
     (T('Specialities'), False, URL('config', 'specialities'),[]),
     (T('Services'), False, URL('config', 'services'),[]),
#     (T('Studies'), False, URL('config', 'studies'),[]),
     (T('Procedures'), False, URL('config', 'procedures'),[]),
#     (T('Units Measures'), False, URL('config', 'units_measures'),[]),
     (T('Relations'), False, URL('config', 'relations'),[]),
     (T('Specialists'), False, URL('config', 'specialists'),[]),
     (T('CIE'), False, URL('config', 'cie'),[]),
     (T('Drugs'), False, URL('config', 'drugs'),[]),
     (T('User Manage'), False, URL('config', 'users_manage'),[]),
     (T('User Groups'), False, URL('config', 'users_groups'),[]),
     (T('User Memberships'), False, URL('config', 'users_membership'),[]),
     (T('Massive Input'), False, URL('config', 'holder_massive_input'),[]),
     (T('Additional Extra Input'), False, URL('config', 'extra_massive_input'),[]),
 ]
),
]
plan = [
(T('Plans'), False, None,
 [
     (T('Plans'), False, URL('plan', 'plan'),[]),
 ]
),
]
account = [
(T('Accounts'), False, None,
 [
     (T('Affiliation'), False, URL('affiliation', 'affiliation'),[]),
 ]
),
]
transaction = [
(T('Transactions'), False, None,
 [
     (T('Care'), False, URL('operation', 'care'),[]),
     # (T('Basic Care'), False, URL('operation', 'basic_care'),[]),
     (T('Full Care'), False, URL('operation', 'identification'),[]),
     (T('Warranties'), False, URL('operation', 'warranty'),[]),
#         (T('Price List'), False, URL('customer_service', 'price_list'),[]),
 ]
),
]
report = [
(T('Reports'), False, None,
 [
     (T('Attentions'), False, URL('report', 'attention'),[]),
#     (T('Diagnostics'), False, URL('report', 'diagnostic'),[]),
#     (T('Prescriptions'), False, URL('report', 'prescription'),[]),
#     (T('Basic Care'), False, URL('operation', 'basic_care'),[]),
#     (T('Warranties'), False, URL('operation', 'warranty'),[]),
#         (T('Price List'), False, URL('customer_service', 'price_list'),[]),
 ]
),

]

response.menu = home
#response.menu += services
if auth.user:
    if auth.has_membership(user_id=auth.user.id, role='root'):
        response.menu += configuration
        response.menu += plan
        response.menu += account
        response.menu += transaction
        response.menu += report
    elif auth.has_membership(user_id=auth.user.id, role='administration'):
#        response.menu = customer_home
        response.menu += plan
        response.menu += account
    elif auth.has_membership(user_id=auth.user.id, role='operator'):
        response.menu += account
    elif auth.has_membership(user_id=auth.user.id, role='specialist'):
        response.menu += transaction
