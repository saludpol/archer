# -*- coding: utf-8 -*-
### we prepend t_ to tablenames and f_ to fieldnames for disambiguity
db.auth_group.update_or_insert(role='root')
db.auth_group.update_or_insert(role='administration')
db.auth_group.update_or_insert(role='account_manager')
db.auth_group.update_or_insert(role='plan_manager')
db.auth_group.update_or_insert(role='operator')
db.auth_group.update_or_insert(role='specialist')
db.auth_group.update_or_insert(role='analyst')
db.auth_group.update_or_insert(role='manager')

# Datos de prueba
if settings.DEVELOPMENT:
    try:
        # Imagenes
        db.imagenology.update_or_insert(code='TOM', name='Tomografía')
        db.imagenology.update_or_insert(code='RES', name='Resonancia magnética')
        db.imagenology.update_or_insert(code='ULT', name='Ultrasonido')
        # Fármacos
        db.drugs.update_or_insert(code='001', name='Panadol')
        db.drugs.update_or_insert(code='002', name='Grabol')
        db.drugs.update_or_insert(code='003', name='Penicilina')
        # Exámenes médicos
        db.tests.update_or_insert(code='HEM', name='Hemograma')
        db.tests.update_or_insert(code='AOR', name='Análisis de orina')
        db.tests.update_or_insert(code='AHE', name='Análisis de heces')
    except:
        pass
