# -*- coding: utf-8 -*-
#__author__ = 'alfonsodg'
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
@auth.requires_login()
def index():
    return dict()

@auth.requires_login()
def error():
    return dict()

@auth.requires_login()
def start():
    user = auth.user.id

    form = SQLFORM.factory(
        Field('affiliate', requires=IS_NOT_EMPTY(), widget=lambda field,value: autocomplete_widget(field, value, base='affiliates', content='document_id,full_name,father_last_name,mother_last_name')),
        Field('cie10', requires=IS_NOT_EMPTY(), widget=lambda field,value: autocomplete_widget(field, value, base='cie', content='code,name')),
    )
    return dict(form=form)

@auth.requires_login()
def identification():
    # TODO
    if True:
        data = {
            # 'cie10': 1,
            'affiliate': 1,
            'medical_center': 1,
            'specialist': 1,
            #'speciality': 1,
            # 'service': 1,
            'internal_document': 12345,
            # 'area': ''
        }
        try:
            speciality = db.specialities(db.specialities.id==data['specialist'])['id']
        except:
            session.flash = T('Error!')
            return dict()
        data.update(dict(speciality=speciality))
        operation_id = db.operations.insert(**data)
        return redirect(URL('operation', 'full_care', args=[operation_id, speciality]))
    return dict()

@auth.requires_login()
def handle_full_care_deletions(operation_id, speciality_id, vars):
    """
    Check if user is trying to delete an existing record. Should be one at a time
    """
    request_keys = filter(lambda key: key.startswith('delete_'), vars)
    if request_keys:
        key = request_keys[0]
        record_id = vars[key]
        if key == 'delete_procedure':
            query = db.operations_procedures.id==record_id
            anchor = '#procedures'
        elif key == 'delete_prescription':
            query = db.operations_prescriptions.id==record_id
            anchor = '#prescriptions'
        elif key == 'delete_imagenology':
            query = db.operations_imagenology.id==record_id
            anchor = '#imagenology'
        elif key == 'delete_laboratory':
            query = db.operations_laboratories.id==record_id
            anchor = '#laboratories'
        elif key == 'delete_hospitalization':
            query = db.operations_hospitalization.id==record_id
            anchor = '#hospitalizations'
        db(query).delete()
        url = '%s%s' % (URL('operation', 'full_care', args=[operation_id, speciality_id]), anchor)
        return redirect(url)

@auth.requires_login()
def full_care():
    operation_id = request.args[0]
    speciality = request.args[1]
    handle_full_care_deletions(operation_id, speciality, request.vars)
    permission = False
    if auth.has_membership('specialist') or auth.has_membership('root'):
        permission = True
    grid_operation = SQLFORM.grid(
        db(db.operations.id==operation_id).query,
        fields=[
            db.operations.affiliate,
            db.operations.operation_date,
            db.operations.medical_center,
            db.operations.internal_document,
            db.operations.speciality,
            db.operations.specialist,
            db.operations.status,
        ],
        csv=False,
        deletable=False,
        editable=False,
        create=False,
        searchable=False,
        sortable=False,
        details=False,
        user_signature=False,
    )

    # Procedimientos
    db.operations_procedures.related_operation.default = operation_id
    db.operations_procedures.register_time.readable = False
    procedures = db(db.operations_procedures.related_operation==operation_id).select()
    db.operations_procedures.related_procedure.requires = IS_IN_DB(db(db.procedures.speciality==speciality), db.procedures.id, '%(name)s')
    form_procedure = SQLFORM(
        db.operations_procedures,
        formstyle='bootstrap',
        submit_button='Agregar'
    ).process()
    form_procedure.element('textarea')['_class'] = 'form-control'
    form_procedure.element('input')['_class'] = 'form-control'
    form_procedure.element('input')['_style'] = 'width:100%'
    form_procedure.element(_type='submit')['_class'] = 'btn btn-success'
    if form_procedure.accepted:
        return redirect(URL('operation', 'full_care', args=[operation_id, speciality])+'#procedures')

    # Prescripciones
    db.operations_prescriptions.related_operation.default = operation_id
    db.operations_prescriptions.register_time.readable = False
    prescriptions = db(db.operations_prescriptions.related_operation==operation_id).select()
    form_prescription = SQLFORM(
        db.operations_prescriptions,
        formstyle='bootstrap',
        submit_button='Agregar'
    ).process()
    form_prescription.element('textarea')['_class'] = 'form-control'
    form_prescription.element('#operations_prescriptions_drug')['_class'] = 'form-control'
    form_prescription.element('#operations_prescriptions_drug')['_style'] = 'width: 100%'
    form_prescription.element('#operations_prescriptions_quantity')['_class'] = 'form-control'
    form_prescription.element('#operations_prescriptions_quantity')['_style'] = 'width: 100%'
    form_prescription.element(_type='submit')['_class'] = 'btn btn-success'
    if form_prescription.accepted:
        return redirect(URL('operation', 'full_care', args=[operation_id, speciality])+'#prescriptions')

    # Imagenologia
    db.operations_imagenology.related_operation.default = operation_id
    db.operations_imagenology.register_time.readable = False
    imagenologies = db(db.operations_imagenology.related_operation==operation_id).select()
    form_imagenology = SQLFORM(
        db.operations_imagenology,
        formstyle='bootstrap',
        submit_button='Agregar'
    ).process()
    form_imagenology.element('textarea')['_class'] = 'form-control'
    form_imagenology.element('input')['_class'] = 'form-control'
    form_imagenology.element('input')['_style'] = 'width: 100%'
    form_imagenology.element(_type='submit')['_class'] = 'btn btn-success'
    if form_imagenology.accepted:
        return redirect(URL('operation', 'full_care', args=[operation_id, speciality])+'#imagenology')

    # Laboratorio
    db.operations_laboratories.related_operation.default = operation_id
    db.operations_laboratories.register_time.readable = False
    laboratories = db(db.operations_laboratories.related_operation==operation_id).select()
    form_laboratory = SQLFORM(
        db.operations_laboratories,
        formstyle='bootstrap',
        submit_button='Agregar'
    ).process()
    form_laboratory.element('textarea')['_class'] = 'form-control'
    form_laboratory.element('input')['_class'] = 'form-control'
    form_laboratory.element('input')['_style'] = 'width: 100%'
    form_laboratory.element(_type='submit')['_class'] = 'btn btn-success'
    if form_laboratory.accepted:
        return redirect(URL('operation', 'full_care', args=[operation_id, speciality])+'#laboratories')

    # Hospitalizacion
    db.operations_hospitalization.related_operation.default = operation_id
    db.operations_hospitalization.register_time.readable = False
    hospitalizations = db(db.operations_hospitalization.related_operation==operation_id).select()
    form_hospitalization = SQLFORM(
        db.operations_hospitalization,
        formstyle='bootstrap',
        submit_button='Agregar'
    ).process()
    form_hospitalization.element('select')['_class'] = 'form-control'
    form_hospitalization.element('textarea')['_class'] = 'form-control'
    form_hospitalization.element('input')['_class'] = 'form-control date'
    form_hospitalization.element('input')['_style'] = 'width: 100%'
    form_hospitalization.element(_type='submit')['_class'] = 'btn btn-success'
    if form_hospitalization.accepted:
        return redirect(URL('operation', 'full_care', args=[operation_id, speciality])+'#hospitalizations')

    return dict(
        grid_operation=grid_operation,
        form_procedure=form_procedure,
        procedures=procedures,
        form_prescription=form_prescription,
        prescriptions=prescriptions,
        form_imagenology=form_imagenology,
        imagenologies=imagenologies,
        form_laboratory=form_laboratory,
        laboratories=laboratories,
        form_hospitalization=form_hospitalization,
        hospitalizations=hospitalizations,
        speciality=speciality,
    )

@auth.requires_login()
def care():
    permission = False
    if auth.has_membership('specialist') or auth.has_membership('root'):
        permission = True
    form = SQLFORM.smartgrid(db.operations,
        linked_tables=[
            # 'operations_diagnosis',
            'operations_procedures',
            'operations_prescriptions',
            'operations_imagenology',
            'operations_laboratories',
            'operations_hospitalization'
        ],
        csv=False,
        deletable=False,
        editable=permission,
        create=permission
    )
    return dict(form=form)

@auth.requires_login()
def basic_care():
    db.operations.status.writable = False
    db.operations.status.readable = False
    form=SQLFORM.factory(db.operations,db.operations_diagnosis,
                         db.operations_prescriptions)
    if form.process().accepted:
        id = db.operations.insert(**db.operations._filter_fields(form.vars))
        form.vars.related_operation=id
        id = db.operations_diagnosis.insert(
            **db.operations_diagnosis._filter_fields(form.vars))
        id = db.operations_prescriptions.insert(
            **db.operations_prescriptions._filter_fields(form.vars))
        response.flash='Registrado!'
    return dict(form=form)

@auth.requires_login()
def warranty():
    return dict()
