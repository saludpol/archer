# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
@auth.requires_login()
def index():
    return dict()

@auth.requires_login()
def error():
    return dict()

@auth.requires_login()
def attention():
    count = db.operations.id.count()
    data = db(db.operations.id>0).select(db.specialities.name,
        count,
        # left=db.specialities.on(db.operations.service==db.specialities.id),
        groupby=db.specialities.name,
        )
    return dict(data=data, count=count)

@auth.requires_login()
def diagnostic():
    count = db.operations_diagnosis.id.count()
    data = db(db.operations_diagnosis.id>0).select(db.cie.code, db.cie.name,
        count,
        left=db.cie.on(db.operations_diagnosis.diagnosis==db.cie.id),
        groupby=db.cie.name,
        )
    return dict(data=data, count=count)
