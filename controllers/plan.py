# -*- coding: utf-8 -*-
#__author__ = 'alfonsodg'
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
@auth.requires_login()
def index():
    return dict()

@auth.requires_login()
def error():
    return dict()

@auth.requires_login()
def plan():
    form = SQLFORM.smartgrid(db.insurance_plans,
                             linked_tables=['insurance_coverage'],
                             csv=False, deletable=False)
    return dict(form=form)
