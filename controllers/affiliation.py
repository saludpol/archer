# -*- coding: utf-8 -*-
#__author__ = 'alfonsodg'
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
@auth.requires_login()
def index():
    return dict()

@auth.requires_login()
def error():
    return dict()

@auth.requires_login()
def affiliation():
    permission = False
    if auth.has_membership('administration') or auth.has_membership('root'):
        permission = True
    form = SQLFORM.smartgrid(db.affiliates,
         linked_tables=['affiliates_relations', 'affiliates_documents',
                        'affiliates_properties', 'affiliates_locations',
                        'affiliates_phones', 'affiliates_mails'
                        ],
#         constraints= dict(customers=query),
         csv=False, deletable=False, editable=permission, create=permission)
    return dict(form=form)
