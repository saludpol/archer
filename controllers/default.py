# -*- coding: utf-8 -*-
import gluon.contrib.simplejson

### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
@auth.requires_login()
def index():
    return dict()

@auth.requires_login()
def error():
    return dict()

@auth.requires_login()
def get_items():
    MINCHARS = 2 # characters required to trigger response
    MAXITEMS = 20 # number of items in response
    query = request.vars.q
    base = request.vars.base
    fields = request.vars.content
    id = db[base]['id']
    if len(query.strip()) > MINCHARS and fields and base:
        content = [db[base][field] for field in fields.split(',')]
        condition = [field.contains(query) for field in content]
        data = []
        for cond in condition:
            data.extend([ row for row in db(cond).select(id, *content,
                limitby=(0, MAXITEMS)).as_list()])
        elements=[]
        for line in data:
            tmp=[]
            for field in fields.split(','):
                tmp.append(line[field])
            #elements.append({'uuid':line['id'], 'value':"%s - %s" % tuple(tmp)})
            elements.append({'uuid':line['id'], 'value': "-".join(tmp)})
            #elements[line['id']] = "%s - %s" % tuple(tmp)
    else:
        elements = {}
    return gluon.contrib.simplejson.dumps(elements)
