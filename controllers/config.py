# -*- coding: utf-8 -*-
#__author__ = 'alfonsodg'
import xlrd
import types
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
@auth.requires_login()
def index():
    return dict()

@auth.requires_login()
def error():
    return dict()

@auth.requires_login()
def medical_center():
    form = SQLFORM.grid(db.medical_centers)
    return dict(form=form)

@auth.requires_login()
def regimens():
    form = SQLFORM.grid(db.regimens)
    return dict(form=form)

@auth.requires_login()
def roles():
    form = SQLFORM.grid(db.roles)
    return dict(form=form)

@auth.requires_login()
def conditions():
    form = SQLFORM.grid(db.conditions)
    return dict(form=form)

@auth.requires_login()
def services():
    form = SQLFORM.smartgrid(db.services,
        linked_tables=[
            'services_prices']
                        )
    return dict(form=form)

# def studies():
#     form = SQLFORM.grid(db.studies)
#     return dict(form=form)

@auth.requires_login()
def procedures():
    form = SQLFORM.grid(db.procedures)
    return dict(form=form)

@auth.requires_login()
def units_measures():
    form = SQLFORM.grid(db.units_measures)
    return dict(form=form)

@auth.requires_login()
def relations():
    form = SQLFORM.grid(db.relations)
    return dict(form=form)

@auth.requires_login()
def specialities():
    form = SQLFORM.grid(db.specialities)
    return dict(form=form)

@auth.requires_login()
def specialists():
    form = SQLFORM.grid(db.specialists)
    return dict(form=form)

@auth.requires_login()
def cie():
    form = SQLFORM.grid(db.cie)
    return dict(form=form)

@auth.requires_login()
def drugs():
    form = SQLFORM.smartgrid(db.drugs,
        linked_tables=[
            'drugs_relations',
            'drugs_prices']
                        )
    return dict(form=form)

@auth.requires_login()
def users_manage():
    form = SQLFORM.grid(db.auth_user,
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_login()
def users_groups():
    form = SQLFORM.grid(db.auth_group,
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_login()
def users_membership():
    form = SQLFORM.grid(db.auth_membership,
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_login()
def main_excel_process(filedata):
    filename = '%s/%s' % (UPLOAD_PATH, filedata)
    book = xlrd.open_workbook(filename, encoding_override="utf_8")
    data_col = []
    #db.affiliates.truncate()
    #db.conditions.truncate()
    # Get Sheets
    for sheet_index in range(book.nsheets):
        sheet = book.sheet_by_index(sheet_index)
        # Get Rows
        for row in range(sheet.nrows)[1:]:
            data_row = []
            # Get Cols
            for col in range(sheet.ncols):
                try:
                    valid_data = sheet.cell_type(row, col)
                    #sample_source = sheet.cell_value(row, col)
                    if valid_data == xlrd.XL_CELL_EMPTY:
                        sample_source = '0'
                    elif valid_data == xlrd.XL_CELL_TEXT:
                        sample_source = sheet.cell_value(row, col)
                    elif valid_data == xlrd.XL_CELL_NUMBER:
                        sample_source = int(sheet.cell_value(row, col))
                    elif valid_data == xlrd.XL_CELL_DATE:
                        sample_source = xlrd.xldate_as_tuple(
                            sheet.cell_value(row, col), book.datemode)
                    elif valid_data == xlrd.XL_CELL_BOOLEAN:
                        sample_source = bool(sheet.cell_value(row, col))
                    else:
                        sample_source = sheet.cell_value(row, col)
                except:
                    sample_source = '0'
                data_row.append(sample_source)
            months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
            situation = db.conditions(db.conditions.name==data_row[6]) or db.conditions.insert(name=data_row[6])#, id=data_row[7])
            role = db.roles(db.roles.name==data_row[8]) or db.roles.insert(name=data_row[8])#, id=data_row[7])
            regimen = db.regimens(db.regimens.name==data_row[11]) or db.regimens.insert(name=data_row[11])#, id=data_row[7])
            if data_row[1] == '0':
                extra_date = '1900-01-01'
            else:
                if data_row[2] not in months:
                    extra_date = '%s-01-01' % data_row[1]
                else:
                    extra_date = '%s-%s-01' % (data_row[1], data_row[2])
            if type(data_row[10]) is not types.TupleType:
                birth_date = '1900-01-01'
            else:
                birth_date = '%s-%s-%s' % (data_row[10][0], data_row[10][1],
                                           data_row[10][2])
            affiliate_main_data = {'additional_document_id':data_row[0],
                             'registration_date':extra_date,
                             'full_name':data_row[3],
                             'alternate_document_id':data_row[4],
                             'document_id':data_row[5],
                             'birth_date': birth_date,
                             }
            try:
                affiliate = db.affiliates.insert(**affiliate_main_data)
            except:
                error_data = str(data_row)
                open('data_error.log', 'a').write(error_data)
            affiliate_extra_data = {
                'affiliate':affiliate,
                'role':role,
                'affiliate_condition':situation,
                'regimen':regimen
            }
            affiliate_property = db.affiliates_properties.insert(
                **affiliate_extra_data)
    return
            #data_col.append(affiliate_main_data)
    #for line in data_col[:30]:
    #    print line


@auth.requires_login()
def extra_excel_process(filedata):
    filename = '%s/%s' % (UPLOAD_PATH, filedata)
    book = xlrd.open_workbook(filename, encoding_override="utf_8")
    data_col = []
    #db.affiliates.truncate()
    #db.conditions.truncate()
    # Get Sheets
    for sheet_index in range(book.nsheets):
        sheet = book.sheet_by_index(sheet_index)
        # Get Rows
        for row in range(sheet.nrows)[1:]:
            data_row = []
            # Get Cols
            for col in range(sheet.ncols):
                try:
                    valid_data = sheet.cell_type(row, col)
                    #sample_source = sheet.cell_value(row, col)
                    if valid_data == xlrd.XL_CELL_EMPTY:
                        sample_source = '0'
                    elif valid_data == xlrd.XL_CELL_TEXT:
                        sample_source = sheet.cell_value(row, col)
                    elif valid_data == xlrd.XL_CELL_NUMBER:
                        sample_source = int(sheet.cell_value(row, col))
                    elif valid_data == xlrd.XL_CELL_DATE:
                        sample_source = xlrd.xldate_as_tuple(
                            sheet.cell_value(row, col), book.datemode)
                    elif valid_data == xlrd.XL_CELL_BOOLEAN:
                        sample_source = bool(sheet.cell_value(row, col))
                    else:
                        sample_source = sheet.cell_value(row, col)
                except:
                    sample_source = '0'
                data_row.append(sample_source)
            main_affiliate = db.affiliates(db.affiliates.alternate_document_id==data_row[1])
            relation = db.relations(db.relations.name==data_row[6]) or db.relations.insert(name=data_row[6])
            if data_row[5] == 'MASCULINO':
                gender = 0
            else:
                gender = 1
            affiliate_main_data = {
                             'full_name':data_row[4],
                             'alternate_document_id':data_row[3],
                             'document_id':data_row[8],
                             'gender':gender,
                             }
            try:
                affiliate = db.affiliates.insert(**affiliate_main_data)
            except:
                error_data = str(data_row)
                open('data_error.log', 'a').write(error_data)
            affiliate_relations = {
                'affiliate':main_affiliate,
                'relation':relation,
                'related_affiliate':affiliate,
            }
            affiliate_relation = db.affiliates_relations.insert(
                **affiliate_relations)
    return


@auth.requires_login()
def holder_massive_input():
    form = SQLFORM.factory(
        Field('process_file', 'upload', label=T('Affiliate Database'),
              uploadfolder=UPLOAD_PATH),
    )
#    form_aux = SQLFORM.grid(db.blacklist)
    if form.process().accepted:
        filename = form.vars.process_file
        file_type = request.vars.process_file.type
        if file_type == EXCEL_FILE or file_type == OLD_EXCEL_FILE:
            status = main_excel_process(filename)
            session.flash = status
        else:
            session.flash = T('Errors in File')

    return dict(form=form)

@auth.requires_login()
def extra_massive_input():
    form = SQLFORM.factory(
        Field('process_file', 'upload', label=T('Affiliate Database'),
              uploadfolder=UPLOAD_PATH),
    )
#    form_aux = SQLFORM.grid(db.blacklist)
    if form.process().accepted:
        filename = form.vars.process_file
        file_type = request.vars.process_file.type
        if file_type == EXCEL_FILE or file_type == OLD_EXCEL_FILE:
            status = extra_excel_process(filename)
            session.flash = status
        else:
            session.flash = T('Errors in File')

    return dict(form=form)
